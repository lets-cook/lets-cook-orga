# Let's cook - Backlog

In diesem Projekt ist das Backlog der "Let's cook" App, in Form eines Scrum Boards zu finden.  
Um auf das Board zu kommen einfach [hier](https://gitlab.com/lets-cook/lets-cook-orga/-/boards) klicken.

## Aufbau des Scrum Boards

Das Board weißt folgende Spalten auf:

* IDEAS (realization unclear)
* PRODUCT BACKLOG (realization planned)
* SPRINT BACKLOG (realization scheduled for next development iteration)
* ACTIVE (realization is currently work-in-progress)
* IMPEDIMENT BACKLOG (realization blocked due to problems or open questions)
* FOR REVIEW (realization finished, incl. documentation & testing, but needs approval)
* DONE (realization closed, can be archived after next (major) milestone)
* REJECTED (no realization planned, can be archived after next (major) milestone)

In dem Board werden Story Cards verwaltet.

## Aufbau einer Story Card

* Kurzbezeichnung der Story Card (als Titel)
* Eine genaue Beschreibung der Story Card, die folgendes enthält:
    * Die User Story
    * Möglicherweise etwaige Vorbedingungen (z.B. Abhängigkeiten zu anderen Story Cards)
    * Tasks zur Abarbeitung
    * Priorität aus Kundensicht nach MoSCoW-Methode (findet sich auch in der Gewichtung wieder)
    * Risiko hinsichtlich Umsetzungserfolg , berechnet durch Riskioindex (genauer: Skript Anforderungsanalyse S. 64f.)
    * Aufwandsschätzung mit „Story Points“ (1 SP = 1 idealer Vollzeitarbeitstag)
    * Auflistung von Akzeptanzkriterien
* Einen Bearbeiter
* Den dazugehörigen Milestone
* Verschiedene *labels*, in unserem Fall ist das nur die momentane Spalte im Board
* Ein angestrebtes Fertigstellungsdatum
* Das Gewicht der Story (1 = "must have", 2 = "should have", 3 = "can have", 4 = "won't have")

## Schema einer User Story

Als **Rolle** möchte ich **Funktionalität** [, damit **Nutzen**]
