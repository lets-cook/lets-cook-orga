# Definition of Done

## Akzeptanz Kriterien
* Es wurden alle Akzeptanzkriterien erfüllt.
* Die Änderungen wurden vom Team akzeptiert.
* Die Dokumentation wurde erweitert.

## Code Kriterien
* Der Code wurde Refactored.
* Der gesamte Code muss für die Projekte im jeweiligen master branch liegen.
* Es müssen essentielle Tests für den Code geschrieben worden sein.
* Essentielle Methoden wurden ausreichend kommentiert.
* Eine lokale statische Codeanalyse wurde ausgeführt und ausgewertet.
* Es wurde ein Code Review durchgeführt.

## CI Kriterien

* Alle Tests müssen durchgelaufen und erfolgreich sein.
* Die neue Version der App steht zum direkten Download bereit.
* Die neue Version der Api wurde auf Cloud Foundry deployet.
