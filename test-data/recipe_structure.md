# Rezept Infos:

## Ingredients

Folgende Units sind möglich:  
*Hinweis:* Einheiten werden mit Bezeichnungen in Klammern abgespeichert!

* Gramm (g)
* Kilogramm (kg)
* Liter (l)
* Milliliter (ml)
* Esslöffel (el)
* Teelöffel (tl)
* Stück (stueck)
* Packung (packung)
* ''

Bei leerer Unit ist eine Zutat gemeint, die man normal nicht als Menge misst (z.B. Gewürze) dann ist die Menge auf 0.

## Instructions

Die Texte zu den einzelnen Schritten sind in einem inneren Objekt, damit dieses um weitere Attribute später erweitert werden kann, ohne einen Breaking Change zu haben.
