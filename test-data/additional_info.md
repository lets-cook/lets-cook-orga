# Zusätzliche Infos für die Rezepte

Diese Infos sind für die nächsten User Stories relevant.

Die Zeit bezieht sich immer auf die Gesamtzeit.  
Die Kalorien beziehen sich immer auf eine Menge von 2 Portionen.  
Bei genannten Zutaten unter den Tags handelt es sich um Allergene.

## Hähnchen Souvlaki
* Zeit: 40 min
* Schwierigkeitsgrad: Mittel
* Kalorien: 619 kcal
* Tags: "Griechisch", "Mittag", "Milch"

## Köttbullar
* Zeit: 30 min
* Schwierigkeitsgrad: Leicht
* Kalorien: 932 kcal
* Tags: "Schwedisch", "Mittag", "Weizen", "Milch"

## Zucchini-Käse-Puffer
* Zeit: 30 min
* Schwierigkeitsgrad: Schwer
* Kalorien: 645 kcal
* Tags: "Vegetarisch", "Mittag", "Milch", "Weizen", "Ei"

## Frikadellen mit Kartoffelstampf
* Zeit: 40 min
* Schwierigkeitsgrad: Mittel
* Kalorien: 1663 kcal
* Tags: "Mittag", "Weizen", "Milch"

## Halloumi mit Linsengemüse
* Zeit: 25 min
* Schwierigkeitsgrad: Mittel
* Kalorien: 735 kcal
* Tags: "Vegetarisch", "Milch"

## Hähnchenburger mit Mozzarella
* Zeit: 45 min
* Schwierigkeitsgrad: Leicht
* Kalorien: 953 kcal
* Tags: "Milch", "Weizen"